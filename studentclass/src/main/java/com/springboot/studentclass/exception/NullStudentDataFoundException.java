package com.springboot.studentclass.exception;

public class NullStudentDataFoundException extends RuntimeException {
	public NullStudentDataFoundException() {
		super("NullStudentDataFoundException");
	}

}