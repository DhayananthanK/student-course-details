package com.springboot.studentclass.exception;

public class StudentNotFoundException extends RuntimeException {
	public StudentNotFoundException() {
		super("StudentNotFoundException");
	}

}