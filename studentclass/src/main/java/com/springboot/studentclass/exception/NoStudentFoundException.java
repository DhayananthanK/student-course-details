package com.springboot.studentclass.exception;


public class NoStudentFoundException extends RuntimeException {
	public NoStudentFoundException() {
		super("NoStudentFoundException");
	}

}
