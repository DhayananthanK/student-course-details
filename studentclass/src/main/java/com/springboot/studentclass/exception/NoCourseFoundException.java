package com.springboot.studentclass.exception;

public class NoCourseFoundException extends RuntimeException {
	public NoCourseFoundException() {
		super("NoCourseFoundException");
	}

}