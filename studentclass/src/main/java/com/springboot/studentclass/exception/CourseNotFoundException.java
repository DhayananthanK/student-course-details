package com.springboot.studentclass.exception;

public class CourseNotFoundException extends RuntimeException {

	public CourseNotFoundException() {
		super("CourseNotFoundException");
	}

}