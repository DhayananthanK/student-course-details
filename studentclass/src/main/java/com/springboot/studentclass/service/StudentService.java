package com.springboot.studentclass.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.studentclass.Model.Student;
import com.springboot.studentclass.exception.NoStudentFoundException;
import com.springboot.studentclass.exception.NullStudentDataFoundException;
import com.springboot.studentclass.exception.StudentNotFoundException;
import com.springboot.studentclass.repository.StudentRepository;



@Service
public class StudentService {

	@Autowired
	StudentRepository stundentRepository;

	public Student saveStudent(Student student) {
		if (student != null) {
			return stundentRepository.save(student);
		} else {
			throw new NullStudentDataFoundException();
		}

	}

	public Student getStudent(Long id) {

		Optional<Student> student = stundentRepository.findById(id);
		if (student.isPresent()) {
			return student.get();
		} else {
			throw new StudentNotFoundException();
		}
	}

	public void deleteStudent(Long id) {
		stundentRepository.deleteById(id);

	}

	public List<Student> getAllStudents() {
		List<Student> students = stundentRepository.findAll();
		if (students.isEmpty()) {
			throw new NoStudentFoundException();
		}
		return students;
	}

	public Student getStudentByName(String name) {
		return stundentRepository.findByName(name);

	}

}

