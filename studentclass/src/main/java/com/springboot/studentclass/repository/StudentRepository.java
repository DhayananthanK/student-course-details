package com.springboot.studentclass.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.springboot.studentclass.Model.Student;


public interface StudentRepository extends JpaRepository<Student, Long> {

	public Student findByName(String name);

}
