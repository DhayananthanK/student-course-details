package com.springboot.studentclass;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class StudentclassApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentclassApplication.class, args);
	}

}
