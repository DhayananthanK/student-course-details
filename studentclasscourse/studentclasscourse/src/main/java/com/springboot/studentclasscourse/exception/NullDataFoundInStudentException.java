package com.springboot.studentclasscourse.exception;

public class NullDataFoundInStudentException extends RuntimeException {
	public NullDataFoundInStudentException() {
		super("NullStudentDataFoundException");
	}

}