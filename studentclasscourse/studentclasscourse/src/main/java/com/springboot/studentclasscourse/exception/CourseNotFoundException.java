package com.springboot.studentclasscourse.exception;

public class CourseNotFoundException extends RuntimeException {
	public CourseNotFoundException() {
		super("CourseNotFoundException");
	}

}
