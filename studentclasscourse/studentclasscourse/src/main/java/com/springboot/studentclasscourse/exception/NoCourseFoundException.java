package com.springboot.studentclasscourse.exception;

public class NoCourseFoundException extends RuntimeException {
	public NoCourseFoundException() {
		super("NoCourseFoundException");
	}

}