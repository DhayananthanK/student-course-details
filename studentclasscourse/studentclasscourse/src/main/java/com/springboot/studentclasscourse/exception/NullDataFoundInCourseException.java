package com.springboot.studentclasscourse.exception;


public class NullDataFoundInCourseException extends RuntimeException {
	public NullDataFoundInCourseException() {
		super("Null Data FOund In Course Exception");
	}

}