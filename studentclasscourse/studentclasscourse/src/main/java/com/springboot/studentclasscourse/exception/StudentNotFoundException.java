package com.springboot.studentclasscourse.exception;

public class StudentNotFoundException extends RuntimeException {
	public StudentNotFoundException() {
		super("StudentNotFoundException");
	}

}

