package com.springboot.studentclasscourse.exception;

public class NoStudentFoundException extends RuntimeException {
	public NoStudentFoundException() {
		super("NoStudentFoundException");
	}

}

